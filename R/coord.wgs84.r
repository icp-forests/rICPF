#' geographical coordinate format conversion from -DDMMSS to -DD.dddd e.g. -8.2345?
#'
#' @param DDMMSS Coordinate in format DDMMSS, e.g. +84530, numeric
#'
#' @return Coordinate in WGS 1984 format, e.g. +8.75..
#' @export
#'
#' @examples x = coord.wgs84(-084512)
#'
coord.wgs84 <- function(DDMMSS) {
  # geographic degrees (DD)
	DD=floor(abs(DDMMSS)/10000)
	# MMSS
	MMSS=abs(DDMMSS)-DD*10000
	# geographic minutes (MM)
	MM=floor(MMSS/100)
	# geographic seconds (SS)
	SS=floor(MMSS-MM*100)
	# WGS 1984 format with decimal degrees
	return(sign(DDMMSS)*(DD+MM/60+SS/3600))
}
