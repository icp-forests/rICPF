#' fread.survey - Import an ICP Forests Survey into data.table
#'
#' Precondition:
#' 1. Submit a Data Request at http://icp-forests.net/page/data-requests
#'    (you will receive an reply within 6 to 8 weeks)
#' 2. Download data from the ICP Forests Database https://icp-forests.org/data
#'    with Mode='SURVEY', e.g. Data Table='dp_dem'
#' 3. Copy the downloaded zipfile to e.g. the R-working directory
#'
#' Usage:
#'    library(data.table)
#'    read.table.survey(path="",surveys="dp")
#'
#' The script will look for an attribute_catalogue_*.csv in a subfolder 'adds'
#' Based on the catalogue it will
#' - imports the data files into data.tables (and data.frames)
#' - imports the dictionaries listed in the attribute catalogue
#' - convert the variables into an appropriate R format
#'    data_type  data.frame    data.table
#'     integer   as.integer()  as.integer()
#'     smallint  as.integer()  as.integer()
#'     numeric   as.numeric()  as.numeric()
#'     date      as.Date()     as.Date()
#'     timestamp as.POSIXct()  as.POSIXct()
#' - null_values are replaced with NA
#' 
#' - latitude and longitude are converted into WGS1984 format
#'   into variables named according to the argutments 
#'   latitutide_name, longitude_name
#' - from code_altitute a variable 'altitude_estimate' with
#'   an estimate in m a.s.l. is derived.
#'   into the variable named according to the argument
#'   altitude_name
#' Extracting of the zip-file is not necessary.
#' If there are more than one zip-file it will select the newest one,
#' unless a specific is specified with the argument zipfile or
#' with including the zipfile name into the path argument
#'
#' @param path Path of the directory within which the zipfile is or to which the *.zip file was extracted to or path including the zipfile-name, default is the working directory
#' @param surveys Surveys, e.g. c("dp","so")
#' @param zipfile Name of a specific Zipfile *.zip that is in the directory path
#' @param pos R environment into which the tables should be loaded
#' @param longitude_name desired name for the converted longitude (WGS 1984),
#' default="longitude" will overwrite longitude
#' @param latitude_name desired name for the converted latitude (WGS 1984),
#' default="longitude" will overwrite latitude
#' @param altitude_name desired name for the approx. altitude in m,
#' default="alitude_estimate"
#' @param sep column separator for import, default=";"
#' @param header consider header for import, default=TRUE
#' @param stringsAsFactors convert strings into factors, default=FALSE
#' @param comment.char char marking comments, default="" is to import all
#' @param quote char for quoting, default="\""
#' @param expand desired dictionary tables that should be merged
#' list of dictionary tables names and dictionary explanatory variable names
#' default expand=c("country","lib_country","code_iso","partner","desc_short","altitude")
#' to keep original structure use expand="none"
#' to have all dictionaries   use expand="all"
#' @param ... further arguments passed to read.table(...) or assign(...)
#'
#' @return nothing, data.frames with the imported data tables and dictionary
#' tables are created in the R environment (pos)
#' listed in the attribute_catalogue in the adds folder
#' @export
#'
#' @examples fread.survey(path="inst/extdata",surveys="dp")
#' @importFrom utils read.table
#' @importFrom data.table fread :=
#' @importFrom stats aggregate
#' @importFrom utils tail unzip
fread.survey <- function (path=getwd(),surveys="dp",
        zipfile="",
        pos=.GlobalEnv,
        longitude_name="longitude",latitude_name="latitude",altitude_name="altitude_estimate",
        sep=";",header=TRUE,stringsAsFactors=FALSE,comment.char="",quote="\"",
        expand=c("default","altitude"),
        forms="all",
        ...) {

   # functions
   # coordinate conversion from -DDMMSS format to e.g. -8.2345?
   coord <- function(x) {
    # geographic degrees
    DD=floor(abs(x)/10000)
    # MMSS
    MMSS=abs(x)-DD*10000
    # geographic minutes
    MM=floor(MMSS/100)
    # geographic seconds
    SS=floor(MMSS-MM*100)
    # WGS 1984 format with decimal degrees
    return(sign(x)*(DD+MM/60+SS/3600))
   }

   # keep only variables in data.frame d that are mentioned in vars
   keepvars <- function(d,vars,stringsAsFactors=FALSE) {
    return(d[,names(d) %in% vars])
   }
   # drop of variables in a dataframe if they are there
   dropvars <- function(d,dvars) {
     return(d[,! names(d) %in% dvars])
   }

   # rename variables of a data.frame
   rename = function(d,renametext,sep="=",seppairs=" ")
   {
      vars=names(d)
      renamepairs=strsplit(renametext,seppairs)[[1]]
      for (renamepair in renamepairs)
      {
         renamevars=strsplit(renamepair,sep)[[1]]
         # loop over variables
         for (i in 1:length(vars))
         {
            # new_name=old_name
            if (vars[i]==renamevars[2]) { vars[i]=renamevars[1]; }
         }
      }
      names(d)=vars
      return(d)
   }

   # instr: get position (pos) of search string (char) in string (x), the x-th occurance (rep)
   instr=function (x,char,rep=1,pos=1) {
      n=nchar(x)
      xpos=base::rep(0,length(x))
      for (j in 1:length(x)) {
          xpos[j]=0
          xrep=0
          if (pos>n[j]) {pos=n[j];}
          for (i in pos:n[j]) {
             if (substr(x[j],i,i)==char) {
                xrep=xrep+1;
                if (xrep==rep) {
                   xpos[j]=i;
                }
             }
          }
      }
      return (xpos)
   }

   # -------------------------------------------------
   # handle arguments
   forms_desired=forms
   
   # library
   require("data.table")
   
   # -------------------------------------------------
   # main
   cat("Import ICP Forests Survey Data Requested from icp-forests.org\n")
   
   # check arguments
   if (!file.exists(path)) {
      cat(paste0("Warning fread.survey: no data found. path ='",path, "' not found. \n"))
      return()
   }

   # ------------------
   # select newest version for each surveys (zip-file or directory)

   # path is a zipfile, e.g. path=C:/temp/23_dp_20231111111111.zip
   if (tolower(substr(path,nchar(path)-3,nchar(path)))==".zip") {
      zipfile=basename(path);
      path=dirname(path);
      cat(paste(zipfile,"\n"));
      survey=substr(zipfile,instr(zipfile,"_",1)+1,instr(zipfile,"_",2)-1);
      survey_list=data.frame(path=path,zipfile=zipfile,survey=survey);
   } else {
      # path is a directory and argument zipfile contains a zipfile name
      if (max(nchar(zipfile)>0)) {
         survey=substr(zipfile,instr(zipfile,"_",1)+1,instr(zipfile,"_",2)-1);
         survey_list=data.frame(path=path,zipfile=zipfile,survey=survey);
      } else {
         # path is not a zipfile and argument zipfile is empty
         if (file.info(path)$isdir) {
            # list zipfiles path/*.zip
            zipfiles=list.files(path=path,pattern="*.zip")
            if (length(zipfiles)>0) {
               zipfiles = data.frame(path=path,zipfile=zipfiles)
               zipfiles = transform(zipfiles,
                                   survey=substr(zipfile,instr(zipfile,"_",1)+1,instr(zipfile,"_",2)-1),
                                   date_download=ifelse(instr(zipfile,"_",3)>0,
                                                        substr(zipfile,instr(zipfile,"_",3)+1,instr(zipfile,".",1)-1),
                                                        substr(zipfile,instr(zipfile,"_",2)+1,instr(zipfile,".",1)-1)
                                   )
               )
            }
            # list path/adds/attribute_catalogue_*.csv
            catalogues=list.files(path=file.path(path,"adds"),pattern="*.csv")
            if (length(catalogues)>0) {
               catalogues=data.frame(path=path,catalogue=catalogues)
               catalogues=transform(subset(catalogues,substr(catalogue,1,19)=="attribute_catalogue"),
                                    survey=substr(catalogue,instr(catalogue,"_",2)+1,instr(catalogue,".",1)-1),
                                    zipfile=""
               )
               for (i in 1:nrow(catalogues)) {
                  catalogues[i,"date_download"]=
                     format(file.info(file.path(catalogues[i,"path"],"adds",catalogues[i,"catalogue"]))$mtime,format="%Y%m%d%H%M%S")
               }
            }
            # merge zipfiles and catalogues
            if (length(zipfiles)>0 & length(catalogues)>0) {
               survey_list0= merge(zipfiles,catalogues,by=c("survey","date_download","path","zipfile"),
                  all=TRUE,stringsAsFactors=FALSE)
            } else {
               if (length(zipfiles)>0) {
                  survey_list0=zipfiles;
               } else {
                  survey_list0=catalogues;
               }
            }
            if (length(survey_list0)>0) {
               # find newest data per survey, either a zipfile or the data catalogue file
               survey_list0=survey_list0[order(survey_list0[,"survey"],survey_list0[,"date_download"]),]
               survey_list1=aggregate(survey_list0[,"path"],by=list(survey_list0[,"survey"]),FUN=tail,n=1)
               names(survey_list1)=c("survey","path")
               survey_list2=aggregate(survey_list0[,"zipfile"],by=list(survey_list0[,"survey"]),FUN=tail,n=1)
               names(survey_list2)=c("survey","zipfile")
               survey_list3=aggregate(survey_list0[,"date_download"],by=list(survey_list0[,"survey"]),FUN=tail,n=1)
               names(survey_list3)=c("survey","date_download")
               survey_list=merge(survey_list1,survey_list2,by="survey")
               survey_list=merge(survey_list,survey_list3,by="survey")
               # filter surveys using the argument surveys
               if (max(nchar(surveys)>0)) {
                  survey_list = subset(survey_list,survey %in% surveys)
               }
            } else {
               cat (paste0("Warning fread.survey: no data found in path='",path,"'.\nThere should be aither an attribute catalogue in a subdirectory adds/attribute_catalogue_{survey}.csv or a zip-file in this directory."))
            } # endif length survey_list0
         } else {
            cat (paste0("Warning fread.survey: no data found. path='",path,"' is neither a directory nor a zipfile.\nThe argument path should point to the directory holding a zipfile or to which the zipfile was extracted to. "))
         } # endif path is a directory and zipfile is empty
      } # endif zipfile is given
   } # endif path is a zipfile

  # ------------------
  # import surveys
  
  # loop survey_list
  if (nrow(survey_list)==0){
     cat(paste0("Warning fread.survey: no data found for the surveys='",paste(surveys,sep="','"),"'\n"));
     if (nrow(survey_list0)>0) {
        cat(paste("(There is only data for the surveys '",paste(survey_list0$survey,sep="",collapse="','"),"'.)\n"));
     }
  } else {
  for (i in 1:nrow(survey_list)) {
   path=survey_list[i,"path"];
   zipfile=survey_list[i,"zipfile"];
   survey=survey_list[i,"survey"];

   # attribute catalogue
   filename=paste0("adds/","attribute_catalogue_",survey,".csv")
   if (max(nchar(zipfile)>0)) {
           file.found=max(filename %in% unzip(file.path(path,zipfile),list=TRUE)[,"Name"])
   } else {
           file.found=file.exists(file.path(path,filename))
   }
   if(file.found) {
         # if zipfile
         if (max(nchar(zipfile)>0)) {
            attribute_catalogue=fread(unzip(file.path(path,zipfile),filename,...),
               sep=sep,header=header,stringsAsFactors=stringsAsFactors,
               quote=quote, ...);
         } else {
            attribute_catalogue=fread(file.path(path,filename),
               sep=sep,header=header,stringsAsFactors=stringsAsFactors,
               quote=quote, ...);
         } # endif zipfile
         # attribute_catalogue_{survey}
         assign(paste0("attribute_catalogue_",survey),attribute_catalogue,pos=pos, ...);

         # list data tables (forms)
         forms=unique(attribute_catalogue$table_name)

         # restrict to desired forms except forms="all"
         if ((max(forms_desired=="all"))) {
            forms=forms 
         } else {
            forms0=forms
            forms=forms0[forms0 %in% forms_desired]         
         }

         # loop over data_tables
         for (form in forms) {
                  # print name of data_table (form)
                 cat(form,":\n")

                 # read csv-file into a temporary data.frame (temp_table)
                 if (exists("temp_table")) {remove(temp_table);}
                 filename <- paste0(form,".csv")
                 formfilename=filename
                 # if zipfile
                 if (max(nchar(zipfile)>0)) {
                    # fallback in case of '#' in other_obs
                    temp_table=tryCatch({
                       fread(unzip(file.path(path,zipfile),filename,...),
                         sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                         quote=quote, ...);
                    }, error = function(e) {
                       message(paste0("  ",formfilename," is read with read.table, after an error with fread"))
                       return(
                       read.table(unzip(file.path(path,zipfile),filename,...),
                         sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                         comment.char=comment.char,quote=quote, ...)
                       );
                    })
                 } else {
                    # fallback in case of '#' in other_obs
                    temp_table=tryCatch({
                       fread(file.path(path,filename),
                         sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                         quote=quote, ...);
                    }, error = function(e) {
                       message(paste0("  ",formfilename," is read with read.table, after an error with fread"))
                       return(
                       read.table(file.path(path,filename),
                         sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                         quote=quote, ...)
                       );
                    })
                 } # endif zipfile

                 # loop columns attributes
                 vars <- names(temp_table)
                 for (var in vars) {
                         cat(paste("  ",var))
                         # extract attribute from catalogue
                         attribute=subset(attribute_catalogue, table_name==form & column_name==var)

                         # convert data types
                         if (attribute$data_type %in% c("smallint","integer")) {
                            if(!is.integer(temp_table[[var]])) {
                                 # integer
                                 eval(substitute(temp_table[,x:=as.integer(x)],list(x=as.symbol(var))))
                            }
                         }
                         if (attribute$data_type %in% c("numeric")) {
                            if(!is.numeric(temp_table[[var]])) {
                                 # numeric
                                 eval(substitute(temp_table[,x:=as.numeric(x)],list(x=as.symbol(var))))
                            }
                         }
                         if (attribute$data_type == "date" & attribute$unit_format == "DDMMYY") {
                            if(is.numeric(temp_table[[var]]) | is.character(temp_table[[var]])) {
                                 # date
                                 eval(substitute(temp_table[,x:=as.Date(x)],list(x=as.symbol(var))))
                            }
                         }
                         if (attribute$data_type == "timestamp without time zone") {
                            if(is.numeric(temp_table[[var]]) | is.character(temp_table[[var]])) {
                                 # timestamp
                                 eval(substitute(temp_table[,x:=as.POSIXct(x)],list(x=as.symbol(var))))
                            }
                         }
                         
                         # null_value to NA
                         if (nchar(attribute$null_value)>0 & (attribute$data_type %in% c("smallint","integer","numeric"))) {
                            if (attribute$null_value!="NOT DEFINED") {
                                 eval(substitute(temp_table[,x:=fifelse(x==as.numeric(attribute$null_value),as.numeric(NA),x)],list(x=as.symbol(var))))
                                 #temp_table[,c(var):=ifelse(get(var)==as.numeric(attribute$null_value),as.numeric(NA),get(var))]
                            }
                         }
                         

                         # convert geographical position
                         #   consider longitude_name, etc. to overwrite or create new variables
                         if (var=="longitude" & attribute$unit_format == "+/-DDMMSS" & nchar(altitude_name)>0) {
                                 # convert to decimal degrees
                                 temp_table[,(longitude_name):=coord(longitude)];
                                 cat(" - ")
                                 if (longitude_name!="longitude") cat(paste0(" ",longitude_name))
                                 cat("converted to WGS1984 format")
                         }
                         if (var=="latitude"  & attribute$unit_format == "+DDMMSS" & nchar(latitude_name)>0) {
                                 # convert to decimal degrees
                                 temp_table[,(latitude_name):=coord(latitude)];
                                 cat(" - ")
                                 if (latitude_name!="latitude") cat(paste0(" ",latitude_name))
                                 cat("converted to WGS1984 format")
                         }
                         if (var=="code_altitude" & nchar(altitude_name)>0) {
                                 # convert to estimated altitude in m a.s.l. (lower boundery of altitude classes)
                                 temp_table[,(altitude_name):=(code_altitude-1)*50];
                         }

                         # add dictionaries fields
                         # extract dictionary name from attribute_catalogue
                         dictionary <- attribute$dictionary

                         # dictionary available and desired
                         if (nchar(dictionary)>0) {
                            cat(paste0(" (",dictionary,") "))
                            # import dictionary table (dict_table)
                            if (exists("dict_table")) {remove(dict_table);}
                            filename = paste0("adds/dictionaries/",dictionary,".csv")
                            # zipfile
                            if (nchar(zipfile)>0) {
                               dict_table <- read.table(unzip(file.path(path,zipfile),filename,...),
                                  sep=";",header=TRUE,stringsAsFactors=FALSE,
                                  comment.char=comment.char,quote="\"");
                            } else {
                               dict_table <- read.table(file.path(path,filename),
                                  sep=";",header=TRUE,stringsAsFactors=FALSE,
                                  comment.char=comment.char,quote="\"");
                            } # endif zipfile

                            # prepare dictionary table and store dictionary to memory
                            dict <- dict_table
                            assign(dictionary,dict_table,pos=pos)

                            # adjust dictionary table d_partner
                            if (var=="partner_code" & exists("d_country")) {
                               dict <- rename(dict,"code_country=country_code");
                               d_country_1=rename(d_country,"code_country=code");
                               dict=merge(dict,d_country_1[,c("code_country","code_iso")],by="code_country",all.x=TRUE)
                               dict=transform(dict,
                                              partner=ifelse(code>100,
                                                             ifelse(code==1301,paste0(code_iso,"_SW"),
                                                                    paste0(code_iso,"_",toupper(substr(desc_short,4,5)))
                                                             ),
                                                             code_iso),
                                              stringsAsFactors=FALSE)
                            }

                            # merge if variable is mentioned in expand or expand == "all" or "default"
                            # if expand=="default" merge partner_code for plot files only
                            if (var %in% expand | substr(dictionary,3,50) %in% expand | "all" %in% expand |
                                ("default" %in% expand & var=="partner_code" &
                                 form %in% c("bd_gpl","c1_trc","f1_plf","s1_pls","y1_pl1","fo_plf","dp_pld",
                                             "aq_pac","cc_trc","gb_pgb","gr_pli","gv_plv","la_pla","lf_lfp",
                                             "mm_plm","oz_pll","si_plt","so_pls","ss_pss"))) {


                               # keep dictionary fields mentioned in expand or keep all if expand contains 'all'
                               if (!("all" %in% expand)) {
                                  dict = keepvars(dict,c("code","valid_from_survey_year","valid_to_survey_year",
                                                         "description","desc_short","value_min","value_max",
                                                         "partner",expand))
                                  fields=names(dict)[!(names(dict) %in% c("code","valid_from_survey_year","valid_to_survey_year"))]
                                  cat(" - ")
                               }

                               # print special conversions
                               if (var=="code_altitude" & nchar(altitude_name)>0) cat(paste0(" ",altitude_name))

                               # rename variables in dictionary table before merge
                               # rename 'code' variable in dictionary to the name of the variable in the data table
                               names(dict)[1] = var

                               # loop 'explanatory' dictionary variables
                               dictvars=names(dict)
                               for (ivar in 2:length(dictvars)) {
                                  dictvar=dictvars[ivar]
                                  # default
                                  if (! dictvar %in% c("valid_from_survey_year","valid_to_survey_year","partner")) {
                                     names(dict)[ivar] = paste0(var,"_",names(dict)[ivar]);
                                  }
                                  # special cases
                                  # variables 'value_min', 'value_max' to {var}_min, {var}_max, e.g. d_altitude
                                  if (dictvar %in% c("value_min")) names(dict)[ivar] = paste0(var,"_min")
                                  if (dictvar %in% c("value_max")) names(dict)[ivar] = paste0(var,"_max")
                                  if (dictvar %in% c("value_min") & substr(var,1,4)=="code") {
                                     names(dict)[ivar] = paste0(substr(var,6,nchar(var)),"_min")
                                  }
                                  if (dictvar %in% c("value_max") & substr(var,1,4)=="code") {
                                     names(dict)[ivar] = paste0(substr(var,6,nchar(var)),"_max")
                                  }
                                  # variable 'country'
                                  if (dictvar=="lib_country") names(dict)[ivar]="country"
                                  if (dictvar=="code_iso") names(dict)[ivar]="country_iso"
                                  # valid_from ....
                                  if (dictvar %in% c("valid_from_survey_year","valid_to_survey_year")) {
                                     names(dict)[ivar]=dictvar
                                  } else {
                                     # print dictionaries name
                                     if (!(var=="partner_code" & dictvar=="description")) {
                                        cat(paste0(" ", names(dict)[ivar]))
                                     }
                                  }
                               } # end loop dictionary variables
                               # remove partner_code_description if not explicitly mentioned
                               if (! "partner_code_description" %in% expand) {
                                  dropvars(dict,"partner_code_description")
                               }
                               if (! "partner_code_desc_short" %in% expand) {
                                  dropvars(dict,"partner_code_desc_short")
                               }

                               # merge the dictionary to data table
                               temp_table <- merge(temp_table, dict, by=var, all.x=TRUE)

                               # remove dictionary entries not valid for the current survey year
                               # surveys all except SI, Y1
                               if ("valid_from_survey_year" %in% names(temp_table) &
                                   "survey_year" %in% names(temp_table)) {
                                  temp_table <- subset(temp_table,
                                     (is.na(valid_from_survey_year) | survey_year>=valid_from_survey_year) &
                                     (is.na(valid_to_survey_year) | survey_year<=valid_to_survey_year))
                                  temp_table <- subset(temp_table, ,-c(valid_from_survey_year,valid_to_survey_year))
                               }

                               # surveys SI, Y1 last_year instead of survey_year
                               if ("valid_from_survey_year" %in% names(temp_table) &
                                   "last_year" %in% names(temp_table)) {
                                  temp_table <- subset(temp_table,
                                     (is.na(valid_from_survey_year) | last_year>=valid_from_survey_year) &
                                     (is.na(valid_to_survey_year) | last_year<=valid_to_survey_year))
                                  temp_table <- subset(temp_table, ,-c(valid_from_survey_year,valid_to_survey_year))
                               }
                             } # endif merge dictionary fields

                         } # endif dictionary available and desired

                         # next line
                         cat("\n")
                 } # end loop columns

                 # rename the temporary data.frame to the form name (environment pos)
                 assign(form,temp_table,pos=pos, ...);
                 # delete temporary table
                 if (exists("temp_table")) {remove(temp_table);}

                 # print
                 if (nchar(zipfile)>0) {
                    cat("  imported from ",formfilename," in ",file.path(path,zipfile),"\n");
                 } else {
                    cat("  imported from ",file.path(path,formfilename),"\n");
                 }
                 
                 # ---------------------------------------------------
                 # form LQA: import ring-test information 
                 if (substr(form,4,6)=="lqa") {
                    
                    # ---------------
                    # {survey}_rt.csv
                    form1=paste0("rt_",survey)
                    cat(form1,":\n")
                    if (exists("temp_table")) {remove(temp_table);}
                    filename <- paste0("adds/",form1,".csv")
                    formfilename=filename
                    # if zipfile
                    if (max(nchar(zipfile)>0)) {
                       # fallback in case of '#' in other_obs
                       temp_table=tryCatch({
                          fread(unzip(file.path(path,zipfile),filename,...),
                            sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                            quote=quote, ...);
                       }, error = function(e) {
                          message(paste0("  ",formfilename," is read with read.table, after an error with fread"))
                          return(
                          read.table(unzip(file.path(path,zipfile),filename,...),
                            sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                            comment.char=comment.char,quote=quote, ...)
                          );
                       })
                    } else {
                       # fallback in case of '#' in other_obs
                       temp_table=tryCatch({
                          fread(file.path(path,filename),
                            sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                            quote=quote, ...);
                       }, error = function(e) {
                          message(paste0("  ",formfilename," is read with read.table, after an error with fread"))
                          return(
                          read.table(file.path(path,filename),
                            sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                            quote=quote, ...)
                          );
                       })
                    } # endif zipfile
                 
                    # rename the temporary data.frame to the form name (environment pos)
                    assign(form1,temp_table,pos=pos, ...);
                    # delete temporary table
                    if (exists("temp_table")) {remove(temp_table);}

                    # print
                    if (nchar(zipfile)>0) {
                       cat("  imported from ",formfilename," in ",file.path(path,zipfile),"\n");
                    } else {
                       cat("  imported from ",file.path(path,formfilename),"\n");
                    }
                    # end {survey}_rt.csv
                    # --------------------

                    # ---------------
                    # qif_{survey}.csv
                    form1=paste0("qif_",survey)
                    cat(form1,":\n")
                    if (exists("temp_table")) {remove(temp_table);}
                    filename <- paste0("adds/",form1,".csv")
                    formfilename=filename
                    # if zipfile
                    if (max(nchar(zipfile)>0)) {
                       # fallback in case of '#' in other_obs
                       temp_table=tryCatch({
                          fread(unzip(file.path(path,zipfile),filename,...),
                            sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                            quote=quote, ...);
                       }, error = function(e) {
                          message(paste0("  ",formfilename," is read with read.table, after an error with fread"))
                          return(
                          read.table(unzip(file.path(path,zipfile),filename,...),
                            sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                            comment.char=comment.char,quote=quote, ...)
                          );
                       })
                    } else {
                       # fallback in case of '#' in other_obs
                       temp_table=tryCatch({
                          fread(file.path(path,filename),
                            sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                            quote=quote, ...);
                       }, error = function(e) {
                          message(paste0("  ",formfilename," is read with read.table, after an error with fread"))
                          return(
                          read.table(file.path(path,filename),
                            sep=sep,header=header,stringsAsFactors=stringsAsFactors,
                            quote=quote, ...)
                          );
                       })
                    } # endif zipfile
                 
                    # rename the temporary data.frame to the form name (environment pos)
                    assign(form1,temp_table,pos=pos, ...);
                    # delete temporary table
                    if (exists("temp_table")) {remove(temp_table);}

                    # print
                    if (nchar(zipfile)>0) {
                       cat("  imported from ",formfilename," in ",file.path(path,zipfile),"\n");
                    } else {
                       cat("  imported from ",file.path(path,formfilename),"\n");
                    }
                    # end qif_{survey}.csv
                    # --------------------
                 
                 } # end if form LQA
                 # ------------------------------------------
                 
         } # end loop forms

      # print links per Survey
      cat(paste0("Online-Documentation for Survey '",toupper(survey),"' see\n",
         "   https://icp-forests.org/documentation/Surveys/",toupper(survey),"/index.html\n")); 
      cat(paste0("Data Accompagnyng Reports-Questionnaires (DAR-Q) and Free-Files for '",toupper(survey),"' see \n",
         "   https://icp-forests.org/documentation-adds/free-files/",survey,".zip\n")); 
      cat("\n")

   } else {
      # file not found
      if (nchar(zipfile)>0) {
         # zipfile not for this survey
         cat("Warning: catalogue",filename,"for survey ",survey,"not found in",file.path(path,zipfile),"- zipfile not imported.\n");
         # determine survey of zipfile from content
         files=unzip(file.path(path,zipfile),list=TRUE)
	 catalogue_file=subset(files,substr(Name,1,24)=="adds/attribute_catalogue","Name")[,"Name"]
	 zipfile_survey=substr(catalogue_file,26,27)
	 cat("Info:    zipfile",file.path(path,zipfile),"is for survey",zipfile_survey," and not for ",survey);
      } else {
         cat("Warning: catalogue",filename,"for survey",survey,"not found in",path,"- survey not imported.\n");
         cat("Info:    extract downloaded zipfile to ",path," or add argument 'zipfile=...'");
      }
   } # endif file not found

  } # end loop survey_list
  } # end if survey_list empty
  # ------------------
  
  # print links
  cat(paste0("ICP Forests Manuals, Publication etc see \n",
     "   http://icp-forests.net/\n"));
  cat(paste0("Experts per Survey and Partner see \n",
     "   http://icp-forests.net/page/expertlist \n"));
  cat(paste0("Intellectual Property and Publicaton Policy see Manual Part I or \n",
     "   https://storage.ning.com/topology/rest/1.0/file/get/9870274098","\n"));

  # return survey_list
  cat("\n")
  return(survey_list)

} # end function
