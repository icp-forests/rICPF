#' geographical coordinate conversion from -DD.ddddd to -DDMMSS
#'
#' @param x Coordinate in WGS 1984 format, e.g. -8.75, numeric
#'
#' @return Coordinate in DDMMSS format, e.g. -084500, numeric
#' @export
#'
#' @examples latitude = coord.ddmmss(45.654)
#'
coord.ddmmss <- function(x) {
	# geographic degrees (DD)
	DD=floor(abs(x))
	# geographic minutes (MM)
	MM=floor(abs(x)*60-DD*60)
	# geographic seconds (SS)
	SS=floor(abs(x)*3600-DD*3600-MM*60)
	# -DDMMSS format numeric
	return(sign(x)*(DD*10000+MM*100+SS))
}

