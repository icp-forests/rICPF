#' instr: get position (pos) of search string (char) in string (x), the i-th occurance (rep)
#'
#' @param x string
#' @param char character pattern
#' @param rep replication
#' @param pos position
#'
#' @return position of i-th occurence of pattern (char) in string (x)
#' @export
#'
#' @examples x=instr("123_dp_321","_",1)
instr=function (x,char,rep=1,pos=1) {
    n=nchar(x)
    xpos=base::rep(0,length(x))
    for (j in 1:length(x)) {
          xpos[j]=0
          xrep=0
          if (pos>n[j]) {pos=n[j];}
          for (i in pos:n[j]) {
             if (substr(x[j],i,i)==char) {
                xrep=xrep+1;
                if (xrep==rep) {
                   xpos[j]=i;
                }
             }
          }
    }
    return (xpos)
}
