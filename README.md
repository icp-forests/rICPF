# rICPF - Tools to import ICP Forests Data into R


## Background

### ICP Forests (http://www.icp-forests.org)
The International Cooperative Programme on Assessment and Monitoring of Long-Range Transboundary Air Pollution Effects on Forests (ICP Forests) has been established under the auspices of the UN-ECE Air Convention (1979). The countries of the United Nations Economic Council of Europe (UNECE) agreed to cooperatively investigate effects on forests. It includes an annual survey of forest condition on a representative on a Pan-European 16x16 km grid (Level I plots) and investigation of cause-effect relationships with intensive monitoring of forest forest ecosystems plots (Level II plots). 

### ICP Forests Manual (http://icp-forests.net/page/icp-forests-manual)
Expert Panels have developped harmonised methods which are described in the ICP Forests Manual. http

### ICP Forests Database
The participating countries (or partners within a countries) submit the data annual to a collaborative database at the programme coordination centre (PCC). There is an expert list with contact points for each Survey and Country/Partner on http://icp-forests.net/page/expertlist.
The ICP Forests data base structured into Surveys, with well defined files (Forms), codes (Dictionaries) and further explanations (Explanatory items). 

### ICP Forests Data Requests http://icp-forests.net/page/data-requests
For rights and obligations of data supply, request and use, refer to the Intellectual Property Policy of ICP Forests that can be found in the Annex of the ICP Forests Manual Part I. To request data fill the PDF-Form available on the ICP Forests website and send it to PCC. 
Typically, you will get access details within 6 weeks. 

## Features rICPF 

When using the option 'SURVEY' downloaded data comes as one Zip-File per Survey that includes all Forms data as well as an 'adds' folder with all details such as an attribute catalogue, all dictionary tables and further information. 


rICPF provides tools to facilitate import and use of ICP Forests data

- import all files directly from downloaded zip-file into data.frame or data.table
- select the latest zip-file for each survey
- convert all columns into the format indicated in the attribute cataloge
- open all dictionary tables
- option to automatically add code explanations (expand="code_tree_species")
- provides links to online-documentation, manual, expert list, free files. 


### Main Functions 

- read.table.survey(path="c:/mydir",surveys="dp", ...) import from zip-file into data.frame
- fread.survey(path="c:/mydir",surveys="mm", forms=c("mm_plm","mm_mem"), ...) import from zip-file into data.table

### Installation
The package can be installed from a local or remote zip-file rICPF_{version}.zip or from the source code rICPF_{version}.tar.gz


> install.packages("http://waldner.internet-box.ch/icp-forests/rICPF_0.2.8.zip",repos=NULL)

### Usage

1. Browser: download data of the survey DP from ICP Forests Database using the option 'SURVEY'. A zip-file will be saved. 
2. Explorer: copy the downloaded zip-files into a directory of your choice on your computer
3. Import in R: 
     > fread.survey(path="C:/mydirectory", surveys="dp")



## Acknowledgment
We highly acknowledge the well structured and well documented work of the data infrastructure team at the PCC of ICP Forests in Eberswalde, mainly Till Kirchner, which makes it possible to develop efficient tools. 

Status & Contributing: The package is under development and might be extended. Informations on errors occuring during the usage or potential improvements are highly welcome. 

Contact: Peter Waldner peter.waldner@wsl.ch WSL